package main

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/tink/go/signature/subtle"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"

	"golang-google-pay-decryption/paymentmethodtoken"
)

const (
	GatewayMarchantID = "gateway:seamoneyapid"
)

type IntermediateSigningKey struct {
	SignedKey  string   `json:"signedKey"`
	Signatures []string `json:"signatures"`
}

type GooglePayToken struct {
	Signature              string                 `json:"signature"`
	IntermediateSigningKey IntermediateSigningKey `json:"intermediateSigningKey"`
	ProtocolVersion        string                 `json:"protocolVersion"`
	SignedMessage          string                 `json:"signedMessage"`
}

func main() {

	var b = &paymentmethodtoken.Builder{}
	b.AddSenderVerifyingKey("MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAElLiHStI30O9lVplgRhBN1AdlQdWyYgjQAcK3vgrqTvxs9WFkLs7CrxGge79+N5AHlklIHwlKu4WKv8E5IFX8DA==")
	b.AddRecipientPrivateKey("fill_this_field")
	b.RecipientId(GatewayMarchantID)
	b.ProtocolVersion("ECv2")
	i := b.Build()

	var token GooglePayToken

	input, err := ioutil.ReadFile("./inputs/input.json")
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(input, &token)
	if err != nil {
		panic(err)
	}

	if err = VerifySig(token, b.GetSenderKeys()[0]); err != nil {
		panic(err)
	}

	res, err := i.HybridDecrypters[0].Decrypt([]byte(token.SignedMessage), []byte("Google"))
	if err != nil {
		panic(err)
	}

	if err = VerifyData(res); err != nil {
		panic(err)
	}

	fmt.Printf("decrypted text:\t %v \n", string(res))
}

func VerifySig(token GooglePayToken, senderKey *ecdsa.PublicKey) error {
	var x []byte
	bs := make([]byte, 4)
	binary.LittleEndian.PutUint32(bs, 6)
	x = append(bs, []byte("Google")...)
	binary.LittleEndian.PutUint32(bs, 4)
	x = append(x, bs...)
	x = append(x, []byte("ECv2")...)
	binary.LittleEndian.PutUint32(bs, uint32(len(token.IntermediateSigningKey.SignedKey)))
	x = append(x, bs...)
	x = append(x, []byte(token.IntermediateSigningKey.SignedKey)...)

	signedStringForIntermediateSigningKeySignature := x

	verifier, err := subtle.NewECDSAVerifierFromPublicKey("SHA256", "DER", senderKey)
	if err != nil {
		log.Fatal(err)
	}

	ok := false
	for _, signature := range token.IntermediateSigningKey.Signatures {
		decodedSignature, err := base64.StdEncoding.DecodeString(signature)
		if err != nil {
			log.Fatal(err)
		}

		err = verifier.Verify(decodedSignature, signedStringForIntermediateSigningKeySignature)
		if err == nil {
			ok = true
		}
	}

	if !ok {
		return errors.New("failed to verify intermediate signing key signature")
	}

	var y []byte
	bs2 := make([]byte, 4)
	binary.LittleEndian.PutUint32(bs2, 6)
	y = append(bs2, []byte("Google")...)
	binary.LittleEndian.PutUint32(bs2, uint32(len(GatewayMarchantID)))
	y = append(y, bs2...)
	y = append(y, []byte(GatewayMarchantID)...)
	binary.LittleEndian.PutUint32(bs2, 4)
	y = append(y, bs2...)
	y = append(y, []byte("ECv2")...)
	binary.LittleEndian.PutUint32(bs2, uint32(len(token.SignedMessage)))
	y = append(y, bs2...)
	y = append(y, []byte(token.SignedMessage)...)
	signedStringForMessageSignature := y

	type signedKey struct {
		KeyVal string `json:"keyValue"`
		KeyExp string `json:"keyExpiration"`
	}

	var buffer signedKey

	err = json.Unmarshal([]byte(token.IntermediateSigningKey.SignedKey), &buffer)
	if err != nil {
		log.Fatal(err)
	}

	// check expiry
	expiry, _ := strconv.Atoi(buffer.KeyExp)

	if time.Now().UnixNano() / int64(time.Millisecond) > (int64(expiry)) {
		return errors.New("expired token")
	}

	decodedSignature2, err := base64.StdEncoding.DecodeString(token.Signature)
	if err != nil {
		log.Fatal(err)
	}

	by, err := base64.StdEncoding.DecodeString(buffer.KeyVal)
	if err != nil {
		log.Fatal(err)
	}

	pubKey, err := x509.ParsePKIXPublicKey(by)
	if err != nil {
		log.Fatal(err)
	}
	ecdsaPubKey2 := pubKey.(*ecdsa.PublicKey)

	verifier2, err := subtle.NewECDSAVerifierFromPublicKey("SHA256", "DER", ecdsaPubKey2)
	if err != nil {
		log.Fatal(err)
	}

	err = verifier2.Verify(decodedSignature2, signedStringForMessageSignature)
	if err == nil {
		return nil
	}

	return errors.New("failed to verify message signature")
}

func VerifyData(data []byte) (err error) {
	type verifyingData struct {
		PayloadGatewayMerchantID string `json:"gatewayMerchantId"`
		PayloadMessageExpiration string `json:"messageExpiration"`
	}

	var vd verifyingData

	err = json.Unmarshal(data, &vd)
	if err != nil {
		return err
	}

	expiry, err := strconv.Atoi(vd.PayloadMessageExpiration)
	if err != nil {
		return err
	}

	if time.Now().UnixNano() / int64(time.Millisecond) > (int64(expiry)) {
		return errors.New("expired token")
	}

	gmid := strings.Split(GatewayMarchantID,":")[1]
	if vd.PayloadGatewayMerchantID != gmid {
		return errors.New(fmt.Sprintf("invalid gateway merchant id: %s", vd.PayloadGatewayMerchantID))
	}

	return nil
}
