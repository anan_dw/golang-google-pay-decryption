package paymentmethodtoken

import (
	"crypto/ecdsa"
	"encoding/hex"
	"log"

	"github.com/google/tink/go/hybrid/subtle"
	"github.com/google/tink/go/tink"
)

type PaymentMethodTokenRecipient struct {
	protocolVersion              string
	senderVerifyingKeysProviders []SenderVerifyingKeysProvider
	HybridDecrypters             []tink.HybridDecrypt
	senderId                     string
	recipientId                  string
}

const (
	ProtocolVersionECv2             = "ECv2"
	AESCtrKeySize                   = 256 / 8
	HMACSha256KeySize               = 256 / 8
)

func New(b *Builder) *PaymentMethodTokenRecipient {
	if b == nil {
		log.Fatal("builder is null")
	}

	paymentMethodTokenRecipient := &PaymentMethodTokenRecipient{}

	if b.protocolVersion != ProtocolVersionECv2 {
		log.Fatal("invalid protocol version: " + b.protocolVersion)
	}
	paymentMethodTokenRecipient.protocolVersion = b.protocolVersion
	if b.senderVerifyingKeysProviders == nil || len(b.senderVerifyingKeysProviders) < 1 {
		log.Fatal("must set at least one way to get sender's verifying key using Builder.AddSenderVerifyingKey")
	}
	paymentMethodTokenRecipient.senderVerifyingKeysProviders = b.senderVerifyingKeysProviders
	paymentMethodTokenRecipient.senderId = b.senderId

	if b.recipientPrivateKeys == nil || len(b.recipientPrivateKeys) < 1 {
		log.Fatal("must add at least one recipient's decrypting key using Builder.AddRecipientPrivateKey")
	}
	paymentMethodTokenRecipient.HybridDecrypters = make([]tink.HybridDecrypt, len(b.recipientPrivateKeys))
	for idx, privateKey := range b.recipientPrivateKeys {
		paymentMethodTokenRecipient.HybridDecrypters[idx] = NewPaymentMethodTokenHybridDecrypt(privateKey)
	}

	return paymentMethodTokenRecipient
}

type Builder struct {
	protocolVersion              string
	senderId                     string
	recipientId                  string
	senderVerifyingKeysProviders []SenderVerifyingKeysProvider
	recipientPrivateKeys         []*subtle.ECPrivateKey
}

func (b *Builder) ProtocolVersion(val string) *Builder {
	b.protocolVersion = val
	return b
}
func (b *Builder) RecipientId(val string) *Builder {
	b.recipientId = val
	return b
}

func (b *Builder) AddSenderVerifyingKey(val string) *Builder {
	if len(b.senderVerifyingKeysProviders) < 1 {
		b.senderVerifyingKeysProviders = make([]SenderVerifyingKeysProvider, 1)
		b.senderVerifyingKeysProviders[0] = &SenderVerifyingKeysProviderImpl{rawKey: val}
	} else {
		b.senderVerifyingKeysProviders = append(b.senderVerifyingKeysProviders, &SenderVerifyingKeysProviderImpl{rawKey: val})
	}
	return b
}

func (b *Builder) AddRecipientPrivateKey(val string) *Builder {
	if len(b.recipientPrivateKeys) < 1 {
		b.recipientPrivateKeys = make([]*subtle.ECPrivateKey, 1)
		b.recipientPrivateKeys[0] = getECPrivateKey(val)
	} else {
		b.recipientPrivateKeys = append(b.recipientPrivateKeys, getECPrivateKey(val))
	}
	return b
}

func getECPrivateKey(hexPrivateKey string) *subtle.ECPrivateKey {
	curve, err := subtle.GetCurve("NIST_P256")
	if err != nil {
		log.Fatal(err)
	}

	by, err := hex.DecodeString(hexPrivateKey)
	if err != nil {
		log.Fatal(err)
	}

	privateKey := subtle.GetECPrivateKey(curve, by)
	return privateKey
}

func (b *Builder) Build() *PaymentMethodTokenRecipient {
	return New(b)
}


func (b *Builder) GetSenderKeys() []*ecdsa.PublicKey {
	res := make([]*ecdsa.PublicKey, len(b.senderVerifyingKeysProviders))
	for idx, keys := range b.senderVerifyingKeysProviders {
		res[idx] = keys.get(b.protocolVersion).(*ecdsa.PublicKey)
	}

	return res
}
