package paymentmethodtoken

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/x509"
	"encoding/base64"
	"log"
)

type SenderVerifyingKeysProvider interface {
	get(protocolVersion string) elliptic.Curve
}

type SenderVerifyingKeysProviderImpl struct {
	rawKey string
}

func (i *SenderVerifyingKeysProviderImpl) get(protocolVersion string) elliptic.Curve {
	by, err := base64.StdEncoding.DecodeString(i.rawKey)
	if err != nil {
		log.Fatal(err)
	}

	pubKey, err := x509.ParsePKIXPublicKey(by)
	if err != nil {
		log.Fatal(err)
	}
	ecdsaPubKey := pubKey.(*ecdsa.PublicKey)
	return ecdsaPubKey
}
