package paymentmethodtoken

import (
	"encoding/base64"
	"encoding/json"
	"log"

	"github.com/google/tink/go/aead"
	hybridSubtle "github.com/google/tink/go/hybrid/subtle"
)

type paymentMethodTokenHybridDecrypt struct {
	recipientKem RecipientKem
}

func NewPaymentMethodTokenHybridDecrypt(key *hybridSubtle.ECPrivateKey) *paymentMethodTokenHybridDecrypt {
	hybridDecrypt := &paymentMethodTokenHybridDecrypt{}
	hybridDecrypt.recipientKem = RecipientKemImpl{privateKey: key}
	return hybridDecrypt
}

func (p paymentMethodTokenHybridDecrypt) Decrypt(signedMessageBytes, ctx []byte) ([]byte, error) {

	var signedMessageMap map[string]string

	err := json.Unmarshal(signedMessageBytes, &signedMessageMap)
	if err != nil {
		log.Fatal(err.Error())
	}

	ephemeralPublicKey, err := base64.StdEncoding.DecodeString(signedMessageMap["ephemeralPublicKey"])
	if err != nil {
		log.Fatal(err)
	}

	encryptedMessage, err := base64.StdEncoding.DecodeString(signedMessageMap["encryptedMessage"])
	if err != nil {
		log.Fatal(err)
	}

	tag, err := base64.StdEncoding.DecodeString(signedMessageMap["tag"])
	if err != nil {
		log.Fatal(err)
	}

	// ciphertext format: ephemeralPublicKey || encryptedMessage || tag
	ciphertext := append(ephemeralPublicKey, append(encryptedMessage, tag...)...)

	pvt := p.recipientKem.GetECPrivateKey()

	helper, err := newRegisterECIESAEADHKDFDemHelper(aead.AES256CTRHMACSHA256KeyTemplate())
	if err != nil {
		log.Fatal(err)
	}

	var salt []byte

	d, err := hybridSubtle.NewECIESAEADHKDFHybridDecrypt(pvt, salt, "SHA256", "UNCOMPRESSED", helper)
	if err != nil {
		log.Fatal(err)
	}

	dt, err := d.Decrypt(ciphertext, ctx)
	if err != nil {
		log.Fatal(err)
	}

	return dt, nil
}

type RecipientKem interface {
	GetECPrivateKey() *hybridSubtle.ECPrivateKey
}

type RecipientKemImpl struct {
	privateKey *hybridSubtle.ECPrivateKey
}

var _ RecipientKem = RecipientKemImpl{}

func (p RecipientKemImpl) GetECPrivateKey() *hybridSubtle.ECPrivateKey {
	return p.privateKey
}
